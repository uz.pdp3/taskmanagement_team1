package uz.pdp.taskManagement.domain.dto;

public record SignUpDTO(String email, String password, String name) {
}
