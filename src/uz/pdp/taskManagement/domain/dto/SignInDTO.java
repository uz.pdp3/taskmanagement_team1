package uz.pdp.taskManagement.domain.dto;

public record SignInDTO(String email, String password) {
}
