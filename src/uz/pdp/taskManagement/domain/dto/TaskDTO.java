package uz.pdp.taskManagement.domain.dto;

import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.domain.model.TaskStatus;

import java.util.UUID;

public record TaskDTO(String title, String description, TaskStatus status, TaskRoles role, UUID id) {
}
