package uz.pdp.taskManagement.domain.model;

public enum TaskRoles {
    BACKEND,
    FRONTEND,
    TESTING;
}
