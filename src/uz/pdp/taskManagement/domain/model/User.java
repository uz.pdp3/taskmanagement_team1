package uz.pdp.taskManagement.domain.model;

public class User extends BaseModel{
    private String email;
    private String password;
    private String name;
    private Roles role;

    public User(){}

    private User(UserBuilder builder){
        this.email = builder.email;
        this.password = builder.password;
        this.name = builder.name;
        this.role = builder.role;
    }

    public static class UserBuilder{
        private String email;
        private String password;
        private String name;
        private Roles role;

        public User build(){
            return new User(this);
        }

        public UserBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder setRole(Roles role) {
            this.role = role;
            return this;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
}
