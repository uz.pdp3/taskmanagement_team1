package uz.pdp.taskManagement.domain.model;

public enum Roles {
    MANAGAR,
    BACKENDLEAD,
    FRONTENDLEAD,
    FRONTDEV,
    BACKDEV,
    TESTER,
    USER
}
