package uz.pdp.taskManagement.domain.model;

import java.util.UUID;

public class Task extends BaseModel{
    private String title;
    private String description;
    private UUID ownerId;
    private UUID receiverId;
    private TaskRoles taskRole;
    private TaskStatus status;

    public Task(){}

    private Task(TaskBuilder builder){
        this.title = builder.title;
        this.description = builder.description;
        this.ownerId = builder.ownerId;
        this.taskRole = builder.taskRole;
        this.status = builder.status;
        this.receiverId = builder.receiverId;
    }

    public static class TaskBuilder{
        private String title;
        private String description;
        private UUID ownerId;
        private UUID receiverId;
        private TaskRoles taskRole;
        private TaskStatus status;

        public Task build(){
            return new Task(this);
        }

        public TaskBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public TaskBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public TaskBuilder setOwnerId(UUID ownerId) {
            this.ownerId = ownerId;
            return this;
        }

        public TaskBuilder setStatus(TaskStatus status) {
            this.status = status;
            return this;
        }

        public TaskBuilder setTaskRole(TaskRoles taskRole) {
            this.taskRole = taskRole;
            return this;
        }

        public TaskBuilder setReceiverId(UUID receiverId) {
            this.receiverId = receiverId;
            return this;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public TaskRoles getTaskRole() {
        return taskRole;
    }

    public void setTaskRole(TaskRoles taskRole) {
        this.taskRole = taskRole;
    }

    public UUID getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(UUID receiverId) {
        this.receiverId = receiverId;
    }
}
