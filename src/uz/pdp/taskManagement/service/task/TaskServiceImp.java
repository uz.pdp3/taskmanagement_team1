package uz.pdp.taskManagement.service.task;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.TaskStatus;
import uz.pdp.taskManagement.repository.task.TaskRepository;
import uz.pdp.taskManagement.repository.task.TaskRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class TaskServiceImp implements TaskService{

    private static final TaskService instance = new TaskServiceImp();

    private final TaskRepository taskRepository = TaskRepositoryImpl.getInstance();

    private TaskServiceImp(){}

    public static TaskService getInstance() {
        return instance;
    }


    @Override
    public ResponseDTO<Task> add(Task t) {
        if(doesTaskExist(t.getTitle(), t.getDescription())){
            return new ResponseDTO<>("\nThis task is already existent\n", 400);
        }
        taskRepository.save(t);
        return new ResponseDTO<>("\nSuccessfully done\n", 200);
    }

    @Override
    public Task getById(UUID id) {
        for (Task task : getAll()) {
            if(Objects.equals(task.getId(), id))
                return task;
        }
        return null;
    }

    @Override
    public ArrayList<Task> getAll() {
        return taskRepository.getAll();
    }

    @Override
    public boolean doesTaskExist(String taskTitle, String taskDescription) {
        for (Task task : getAll()) {
            if(Objects.equals(task.getTitle(), taskTitle) &&
                    Objects.equals(task.getDescription(), taskDescription) &&
                    (task.getStatus() != TaskStatus.DONE || task.getStatus() != TaskStatus.CANCELED))
                return true;
        }
        return false;
    }

    @Override
    public ArrayList<TaskDTO> getUserTasks(UUID id) {
        return taskRepository.getOwnerTasks(id);
    }
}
