package uz.pdp.taskManagement.service.task;

import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface TaskService extends BaseService<Task, Task> {
    boolean doesTaskExist(String taskTitle, String taskDescription);
    ArrayList<TaskDTO> getUserTasks(UUID id);
}
