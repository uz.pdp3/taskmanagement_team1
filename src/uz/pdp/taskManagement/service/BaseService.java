package uz.pdp.taskManagement.service;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;

import java.util.ArrayList;
import java.util.UUID;

public interface BaseService<CD, RD> {
    ResponseDTO<RD> add(CD t);
    RD getById(UUID id);
    ArrayList<RD> getAll();
}
