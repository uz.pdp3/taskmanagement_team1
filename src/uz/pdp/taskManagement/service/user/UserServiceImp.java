package uz.pdp.taskManagement.service.user;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.SignInDTO;
import uz.pdp.taskManagement.domain.dto.SignUpDTO;
import uz.pdp.taskManagement.domain.model.Roles;
import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.domain.model.User;
import uz.pdp.taskManagement.repository.user.UserRepository;
import uz.pdp.taskManagement.repository.user.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class UserServiceImp implements UserService{

    private final UserRepository userRepository = UserRepositoryImpl.getInstance();

    private static final UserService instance = new UserServiceImp();

    private UserServiceImp(){}

    int k = 0;

    public static UserService getInstance() {
        return instance;
    }

    @Override
    public ResponseDTO<User> add(SignUpDTO sign) {
        if(doesUserExists(sign.email())){
            return new ResponseDTO<>("\nThis email has already been registered\n", 400);
        }
        User user = new User.UserBuilder()
                .setEmail(sign.email())
                .setPassword(sign.password())
                .setName(sign.name())
                .setRole((k==0) ? Roles.MANAGAR : Roles.USER)
                .build();
        userRepository.save(user);
        k++;
        return new ResponseDTO<>("\nsuccessfully done!!\n", 200);
    }

    @Override
    public User getById(UUID id) {
        for (User user : getAll()) {
            if(Objects.equals(user.getId(), id)){
                return user;
            }
        }
        return null;
    }

    @Override
    public ArrayList<User> getAll() {
        return userRepository.getAll();
    }

    @Override
        public ResponseDTO<User> signIn(SignInDTO sign) {
            for (User user : userRepository.getAll()) {
                if(Objects.equals(sign.email(), user.getEmail()) &&
                        Objects.equals(sign.password(), user.getPassword())){
                    return new ResponseDTO<>("\nWelcome\n", user, 200);
                }
            }
            return new ResponseDTO<>("\nNot found\n", 404);
        }

    @Override
    public boolean doesUserExists(String email) {
        for (User user : getAll()) {
            if(Objects.equals(user.getEmail(), email))
                return true;
        }
        return false;
    }

    @Override
    public ArrayList<User> getUsersExceptMe(UUID id) {
        ArrayList<User> users = new ArrayList<>();
        for (User user : getAll()) {
            if(!Objects.equals(user.getId(), id) && user.getRole() != Roles.MANAGAR)
                users.add(user);
        }
        return users;
    }

    @Override
    public ArrayList<User> getOnlyDev(Roles role) {
        ArrayList<User> devs = new ArrayList<>();
        for (User user : getAll()) {
            if(Objects.equals(user.getRole(), role))
                devs.add(user);
        }
        return devs;
    }

    @Override
    public ArrayList<User> getOnlyLeads(TaskRoles role) {
        Roles userRole = null;
        if(role == TaskRoles.FRONTEND)
            userRole = Roles.FRONTENDLEAD;
        else
            userRole = Roles.BACKENDLEAD;
        ArrayList<User> leads = new ArrayList<>();
        for (User user : getAll()) {
            if(Objects.equals(user.getRole(), userRole))
                leads.add(user);
        }
        return leads;
    }
}

