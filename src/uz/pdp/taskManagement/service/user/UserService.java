package uz.pdp.taskManagement.service.user;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.SignInDTO;
import uz.pdp.taskManagement.domain.dto.SignUpDTO;
import uz.pdp.taskManagement.domain.model.Roles;
import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.domain.model.User;
import uz.pdp.taskManagement.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface UserService extends BaseService<SignUpDTO, User> {
    ResponseDTO<User> signIn(SignInDTO sign);

    boolean doesUserExists(String email);
    ArrayList<User> getUsersExceptMe(UUID id);

    ArrayList<User> getOnlyDev(Roles role);
    ArrayList<User> getOnlyLeads(TaskRoles role);
}
