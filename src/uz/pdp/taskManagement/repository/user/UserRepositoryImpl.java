package uz.pdp.taskManagement.repository.user;

import uz.pdp.taskManagement.domain.model.User;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository{

    private final ArrayList<User> users = new ArrayList<>();

    private static final UserRepository instance = new UserRepositoryImpl();

    private UserRepositoryImpl(){}

    public static UserRepository getInstance() {
        return instance;
    }

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public User getById(UUID id) {
        for (User user : users) {
            if(Objects.equals(user.getId(), id))
                return user;
        }
        return null;
    }

    @Override
    public ArrayList<User> getAll() {
        return users;
    }

    @Override
    public void removeById(UUID id) {

    }

    @Override
    public void remove(User user) {

    }
}
