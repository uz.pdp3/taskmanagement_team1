package uz.pdp.taskManagement.repository.user;

import uz.pdp.taskManagement.domain.model.User;
import uz.pdp.taskManagement.repository.BaseRepository;

public interface UserRepository extends BaseRepository<User> {

}
