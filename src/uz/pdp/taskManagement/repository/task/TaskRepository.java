package uz.pdp.taskManagement.repository.task;

import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.repository.BaseRepository;

import java.util.ArrayList;
import java.util.UUID;

public interface TaskRepository extends BaseRepository<Task> {

    ArrayList<Task> getTasksByType(TaskRoles role);
    ArrayList<TaskDTO> getOwnerTasks(UUID id);

}
