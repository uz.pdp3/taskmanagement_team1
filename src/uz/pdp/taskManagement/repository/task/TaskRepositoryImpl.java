package uz.pdp.taskManagement.repository.task;

import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.TaskRoles;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class TaskRepositoryImpl implements TaskRepository{

    private final ArrayList<Task> tasks = new ArrayList<>();

    private static final TaskRepository instance = new TaskRepositoryImpl();

    private TaskRepositoryImpl(){}

    public static TaskRepository getInstance() {
        return instance;
    }

    @Override
    public void save(Task task) {
        tasks.add(task);
    }

    @Override
    public Task getById(UUID id) {
        for (Task task : tasks) {
            if(Objects.equals(task.getId(), id))
                return task;
        }
        return null;
    }

    @Override
    public ArrayList<Task> getAll() {
        return tasks;
    }

    @Override
    public void removeById(UUID id) {

    }

    @Override
    public void remove(Task task) {

    }

    @Override
    public ArrayList<Task> getTasksByType(TaskRoles role) {
        ArrayList<Task> tasks = new ArrayList<>();
        for (Task task : getAll()) {
            if(Objects.equals(task.getTaskRole(), role))
                tasks.add(task);
        }
        return tasks;
    }

    @Override
    public ArrayList<TaskDTO> getOwnerTasks(UUID id) {
        ArrayList<TaskDTO> tasks = new ArrayList<>();
        for (Task task : getAll()) {
            if(Objects.equals(task.getOwnerId(), id) || Objects.equals(task.getReceiverId(), id))
                tasks.add( new TaskDTO(task.getTitle(), task.getDescription(), task.getStatus(), task.getTaskRole(),
                        task.getId()));
        }
        return tasks;
    }
}
