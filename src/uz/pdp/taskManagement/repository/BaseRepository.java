package uz.pdp.taskManagement.repository;
import java.util.ArrayList;
import java.util.UUID;

public interface BaseRepository <T>{
    void save(T t);
    T getById(UUID id);
    ArrayList<T> getAll();
    void removeById(UUID id);
    void remove(T t);

}
