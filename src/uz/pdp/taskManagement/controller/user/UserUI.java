package uz.pdp.taskManagement.controller.user;

import uz.pdp.taskManagement.controller.backEndLead.BackLeadUI;
import uz.pdp.taskManagement.controller.developer.DevUI;
import uz.pdp.taskManagement.controller.frontEndLead.FrontEndLeadUI;
import uz.pdp.taskManagement.controller.meneger.ManagerUI;
import uz.pdp.taskManagement.controller.tester.TesterUI;
import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.SignInDTO;
import uz.pdp.taskManagement.domain.dto.SignUpDTO;
import uz.pdp.taskManagement.domain.model.Roles;
import uz.pdp.taskManagement.domain.model.User;

import static uz.pdp.taskManagement.controller.UtilClass.*;

public class UserUI {

    ManagerUI managerUI = new ManagerUI();
    BackLeadUI backLeadUI = new BackLeadUI();
    FrontEndLeadUI frontLeadUI = new FrontEndLeadUI();
    DevUI devUI = new DevUI();
    TesterUI testerUI = new TesterUI();

    public void start() {
        byte choice = 0;
        while (true) {
            System.out.println("1. Sign in\t 2. Sign up\t 0. Exit");
            choice = scanNum.nextByte();
            switch (choice) {
                case 1 -> {
                    signIn();
                }
                case 2 -> {
                    signUp();
                }
                case 0 -> {
                    System.out.println("\nThank you for your cooperation");
                    return;
                }
            }
        }
    }

    private void signUp() {
        System.out.print("E-mail : ");
        String email = scanStr.nextLine();
        System.out.print("Password : ");
        String password = scanStr.nextLine();
        System.out.print("Name : ");
        String name = scanStr.nextLine();
        ResponseDTO<User> response = userService.add(new SignUpDTO(email, password, name));
        System.out.println(response.getMessage());
    }


    private void signIn() {
        System.out.print("Enter E-mail: ");
        String email = scanStr.nextLine();
        System.out.print("Enter password: ");
        String password = scanStr.nextLine();
        ResponseDTO<User> response = userService.signIn(new SignInDTO(email, password));
        if(response.getStatus() == 200){
            System.out.println(response.getMessage());
            typeOfWorkers(response.getData());
        } else
            System.out.println(response.getMessage());
    }

    public void typeOfWorkers(User user){
        if(user.getRole() == Roles.MANAGAR)
            managerUI.ManagerMenu(user);
        else if(user.getRole() == Roles.USER)
            System.out.println("Please contact with your manager to identify your profession!!\n");
        else if(user.getRole() == Roles.BACKENDLEAD)
            backLeadUI.BackLeadMenu(user);
        else if(user.getRole() == Roles.FRONTENDLEAD)
            frontLeadUI.FrontEndLeadMenu(user);
        else if(user.getRole() == Roles.FRONTDEV || user.getRole() == Roles.BACKDEV)
            devUI.devMenu(user);
        else
            testerUI.testerUI(user);
    }
}
