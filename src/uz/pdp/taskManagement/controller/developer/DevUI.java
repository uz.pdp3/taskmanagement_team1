package uz.pdp.taskManagement.controller.developer;

import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.domain.model.User;

import java.util.UUID;

import static uz.pdp.taskManagement.controller.UtilClass.*;
import static uz.pdp.taskManagement.domain.model.TaskStatus.IN_PROGRESS;

public class DevUI {
    public void devMenu(User user){
        byte choice = 0;
        while(true) {
            System.out.println("1. Do tasks\t 2. See tasks\t 0. Exit");
            choice = scanNum.nextByte();
            switch (choice){
                case 1 -> performTask(user.getId());
                case 2 -> seeTasks(user.getId());
                case 0 -> {
                    return;
                }
                default -> System.out.println("\nWrong input\n");
            }
        }
    }

    private void seeTasks(UUID id) {
        System.out.println();
        int i = 1;
        for (TaskDTO task : taskService.getUserTasks(id)) {
            System.out.println(i++ + ". Title : " +task.title() + " | Descr : "+ task.description() +
                    " | Status : " + task.status());
            System.out.println("--------------------------------------------------------------------------");
        }
        System.out.println();
    }

    private void performTask(UUID id) {
        seeTasks(id);
        int x = 1;
        System.out.print("Choose task: ");
        int index = scanNum.nextInt();
        if(index == 0){
            return;
        } else if(index < 0 || index > taskService.getUserTasks(id).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(taskService.getUserTasks(id).get(--index).id());
        task.setStatus(IN_PROGRESS);
        task.setTaskRole(TaskRoles.TESTING);
        System.out.println("\nDone\n");
    }
}
