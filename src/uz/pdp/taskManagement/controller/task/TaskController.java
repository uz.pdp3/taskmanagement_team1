package uz.pdp.taskManagement.controller.task;

import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.User;
import static uz.pdp.taskManagement.controller.UtilClass.*;

public class TaskController {
    public void showAllTasks(User user){
        if(taskService.getUserTasks(user.getId()).isEmpty()){
            System.out.println("\nThere is not any task yet\n");
            return;
        }
        int i = 1;
        for (TaskDTO task : taskService.getUserTasks(user.getId())) {
            System.out.print(i++ + ". Title : " + task.title() + " | Descr : " + task.description());
            System.out.println(" | Status : " + task.status() + " | Role : " + task.role());
            System.out.println("--------------------------------------------------------------------------------");
        }
    }
}
