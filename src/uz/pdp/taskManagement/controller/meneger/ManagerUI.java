package uz.pdp.taskManagement.controller.meneger;

import uz.pdp.taskManagement.controller.task.TaskController;
import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import static uz.pdp.taskManagement.controller.UtilClass.*;

public class ManagerUI {

    TaskController taskController = new TaskController();

    public void ManagerMenu(User user){
        byte choose = 0;
        while (true){
            System.out.println("1. Creat tasks\t2. Assign tasks\t3. See all workers\t4. Change user role\t" +
                    "5. See user's tasks\t0. Exit");
            choose = scanNum.nextByte();
            switch (choose){
                case 1 -> creatTasks(user.getId());
                case 2 -> assignTask(user);
                case 3 -> seeAllWorkers(user.getId());
                case 4 -> updateUserRole(user);
                case 5 -> seeUserTasks(user);
                case 0 -> {
                    return;
                }
                default -> System.out.println("\nWrong input\n");
            }
        }
    }

    private void assignTask(User user) {
        ArrayList<TaskDTO> tasks = taskService.getUserTasks(user.getId());
        taskController.showAllTasks(user);
        if(tasks.isEmpty())
            return;
        System.out.print("\nChoose a task you want to assign (Back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice < 0 || choice > tasks.size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(tasks.get(--choice).id());
        assignToUser(task, user);
    }

    public void assignToUser(Task task, User user){
        seeAllWorkers(user.getId());
        if(userService.getUsersExceptMe(user.getId()).isEmpty()){
            System.out.println("\nCurrently there is no any workers\n");
            return;
        }
        System.out.print("\nChoose a worker(back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0){
            return;
        } else if(choice < 0 || choice > userService.getUsersExceptMe(user.getId()).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        User selectedUser = userService.getUsersExceptMe(user.getId()).get(--choice);
        doesAssignSuccess(task, selectedUser);
    }

    public void doesAssignSuccess(Task task, User user){
        boolean res = false;
        if((user.getRole() == Roles.BACKDEV || user.getRole() == Roles.BACKENDLEAD) &&
                task.getTaskRole() == TaskRoles.BACKEND) {
            res = true;
        } else if((user.getRole() == Roles.FRONTDEV || user.getRole() == Roles.FRONTENDLEAD) &&
                task.getTaskRole() == TaskRoles.FRONTEND){
            res = true;
        } else if(user.getRole() == Roles.TESTER && task.getTaskRole() == TaskRoles.TESTING){
            res = true;
        }
        if(res) {
            task.setReceiverId(user.getId());
            task.setStatus(TaskStatus.ASSIGNED);
            System.out.println("\nSuccessfully assigned\n");
        } else
            System.out.println("\nSorry, you cannot assign task as worker role and task role does not match up!!\n");
    }


    private void seeUserTasks(User user){
        seeAllWorkers(user.getId());
        int x = 1;
        System.out.print("\nChoose user (Back=0) : ");
        int index = scanNum.nextInt();
        if(index == 0){
            return;
        } else if(userService.getUsersExceptMe(user.getId()).isEmpty()){
            System.out.println("\nCurrently no task is assigned!\n");
            return;
        }
        else if(index < 0 || index > userService.getUsersExceptMe(user.getId()).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        User user1 = userService.getUsersExceptMe(user.getId()).get(index - 1);
        for (Task task : taskService.getAll()) {
            if(Objects.equals(task.getReceiverId(), user1.getId()) && task.getStatus() != TaskStatus.DONE){
                System.out.println(x++ + ". Title : " + task.getTitle()+ " | Descr : " +task.getDescription() +
                        " | Status : " + task.getStatus());
                System.out.println("-----------------------------------------------------------");
            }
        }
        System.out.println();
    }

    private void updateUserRole(User currentUser) {
        seeAllWorkers(currentUser.getId());
        System.out.print("\nChoose user(Back=0) : ");
        int index = scanNum.nextInt();
        if(index == 0){
            return;
        } else if(index < 0 || index > userService.getUsersExceptMe(currentUser.getId()).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        User selectedUser = userService.getUsersExceptMe(currentUser.getId()).get(--index);
        System.out.println("\n1. Change role\t0 . Back");
        System.out.print("Choose role : ");
        String str = scanStr.nextLine();
        switch (str) {
            case "1" -> {
                System.out.println("1. BACK-END LEAD\t2. FRONT-END LEAD\t3. TESTER\t4 .  BACK-END DEV\t5. FRONT-END DEV\t0. Back");
                System.out.print("Choose : ");
                String a = scanStr.nextLine();
                switch (a) {
                    case "1" -> selectedUser.setRole(Roles.BACKENDLEAD);
                    case "2" -> selectedUser.setRole(Roles.FRONTENDLEAD);
                    case "3" -> selectedUser.setRole(Roles.TESTER);
                    case "4" -> selectedUser.setRole(Roles.BACKDEV);
                    case "5" -> selectedUser.setRole(Roles.FRONTDEV);
                    case "0" -> {
                        return;
                    }
                    default -> System.out.println("\nWrong input\n");
                }
                System.out.println("\nsuccessfully changed👌\n");
            }
            case "0" -> {}
        }
        selectedUser.setId(UUID.randomUUID());
    }
    private void seeAllWorkers(UUID id) {
        if(userService.getUsersExceptMe(id).isEmpty()){
            System.out.println("\nCurrently there is no any workers\n");
            return;
        }
        int i = 1;
        for (User worker : userService.getUsersExceptMe(id)) {
            System.out.println(i++ + ". Name : " + worker.getName() + " | Role : " + worker.getRole());
            System.out.println("--------------------------------------------------");
        }
    }

    private void creatTasks(UUID id) {
        System.out.println();
        System.out.println("1. Back-end\t2. Front-end\t0. Exit");
        System.out.print("Choose ->(Back=0) : ");
        String choose = scanStr.nextLine();
        switch (choose){
            case "1" -> forBackEnd(id);
            case "2" -> forFrontEnd(id);
            case "0" -> {}
            default -> System.out.println("\nWrong input\n");
        }
    }

    private void forFrontEnd(UUID id) {
        System.out.println();
        System.out.print("Enter task name : ");
        String taskName = scanStr.nextLine();

        System.out.print("Enter task descraption : ");
        String taskText = scanStr.nextLine();

        Task task = new Task.TaskBuilder()
                .setTitle(taskName)
                .setDescription(taskText)
                .setOwnerId(id)
                .setStatus(TaskStatus.CREATED)
                .setTaskRole(TaskRoles.FRONTEND)
                .build();
        ResponseDTO<Task> response = taskService.add(task);
        System.out.println(response.getMessage());
    }

    private void forBackEnd(UUID id) {
        System.out.println();
        System.out.print("Enter task name : ");
        String taskName = scanStr.nextLine();

        System.out.print("Enter task descraption : ");
        String taskText = scanStr.nextLine();

        Task task = new Task.TaskBuilder()
                .setTitle(taskName)
                .setDescription(taskText)
                .setOwnerId(id)
                .setStatus(TaskStatus.CREATED)
                .setTaskRole(TaskRoles.BACKEND)
                .build();
        ResponseDTO<Task> response = taskService.add(task);
        System.out.println(response.getMessage());
    }
}
