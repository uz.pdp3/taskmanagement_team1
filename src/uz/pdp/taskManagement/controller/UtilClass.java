package uz.pdp.taskManagement.controller;

import uz.pdp.taskManagement.repository.task.TaskRepository;
import uz.pdp.taskManagement.repository.task.TaskRepositoryImpl;
import uz.pdp.taskManagement.service.task.TaskService;
import uz.pdp.taskManagement.service.task.TaskServiceImp;
import uz.pdp.taskManagement.service.user.UserService;
import uz.pdp.taskManagement.service.user.UserServiceImp;

import java.util.Scanner;

public interface UtilClass {
    Scanner scanNum = new Scanner(System.in);
    Scanner scanStr = new Scanner(System.in);
    static UserService userService = UserServiceImp.getInstance();
    TaskService taskService = TaskServiceImp.getInstance();
    TaskRepository taskRepository = TaskRepositoryImpl.getInstance();
}
