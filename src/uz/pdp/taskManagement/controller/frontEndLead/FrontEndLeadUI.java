package uz.pdp.taskManagement.controller.frontEndLead;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.*;

import java.util.ArrayList;
import java.util.UUID;

import static uz.pdp.taskManagement.controller.UtilClass.*;
import static uz.pdp.taskManagement.domain.model.TaskStatus.IN_PROGRESS;

public class FrontEndLeadUI {
    public void FrontEndLeadMenu(User user) {
        while (true) {
            System.out.println("1. Create task\t2. See tasks\t3. Do tasks\t4. Assign task\t0. Exit");
            String choose = scanStr.nextLine();

            switch (choose) {
                case "1" -> createTasks(user);
                case "2" -> seeTasks(user.getId());
                case "3" -> performTask(user.getId());
                case "4" -> assignTask(user);
                case "0" -> {
                    return;
                }
                default -> System.out.println("\nWrong input\n");
            }
        }
    }

    private void seeTasks(UUID id) {
        if(taskService.getUserTasks(id).isEmpty()){
            System.out.println("\nCurrently, there is no any task\n");
            return;
        }
        System.out.println();
        int i = 1;
        for (TaskDTO task : taskService.getUserTasks(id)) {
            System.out.println(i++ + ". Title : " + task.title() + " | Descr : " + task.description() +
                    " | Status : " + task.status());
            System.out.println("--------------------------------------------------------------------------");
        }
        System.out.println();
    }

    private void createTasks(User user) {
        System.out.print("Enter task name : ");
        String taskName = scanStr.nextLine();

        System.out.print("Enter task descraption : ");
        String taskText = scanStr.nextLine();

        Task task = new Task.TaskBuilder()
                .setTitle(taskName)
                .setDescription(taskText)
                .setStatus(TaskStatus.CREATED)
                .setOwnerId(user.getId())
                .setTaskRole(TaskRoles.FRONTEND)
                .build();
        ResponseDTO<Task> response = taskService.add(task);
        System.out.println(response.getMessage());
    }

    private void performTask(UUID id) {
        seeTasks(id);
        int x = 1;
        System.out.print("Choose task: ");
        int index = scanNum.nextInt();
        if(index == 0){
            return;
        } else if(index < 0 || index > taskService.getUserTasks(id).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(taskService.getUserTasks(id).get(--index).id());
        task.setStatus(IN_PROGRESS);
        task.setTaskRole(TaskRoles.TESTING);
        System.out.println("\nDone\n");
    }

    private void seeFrontEndDevs(){
        if(userService.getOnlyDev(Roles.FRONTDEV).isEmpty()){
            System.out.println("\nCurrently, there is no any front-end developer\n");
            return;
        }
        int i = 1;
        for (User user : userService.getOnlyDev(Roles.FRONTDEV)) {
            System.out.println(i++ + ". " + user.getName());
            System.out.println("-------------------------------------------------");
        }
    }

    private void assignTask(User user){
        seeTasks(user.getId());
        if(taskService.getUserTasks(user.getId()).isEmpty())
            return;
        System.out.print("Choose task you want to assign (back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice < 0 || choice > taskService.getUserTasks(user.getId()).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(taskService.getUserTasks(user.getId()).get(--choice).id());
        System.out.println();
        assignToDev(task);
    }

    private void assignToDev(Task task){
        ArrayList<User> frontDevs = userService.getOnlyDev(Roles.FRONTDEV);
        seeFrontEndDevs();
        if(frontDevs.isEmpty())
            return;
        System.out.print("\nChoose worker (back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice < 0 || choice > frontDevs.size()){
            System.out.println("\nWrong input\n");
            return;
        }
        task.setReceiverId(frontDevs.get(--choice).getId());
        System.out.println("\nSuccessfully assigned\n");
    }
}


