package uz.pdp.taskManagement.controller.backEndLead;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Roles;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.domain.model.User;

import java.util.ArrayList;
import java.util.UUID;

import static uz.pdp.taskManagement.controller.UtilClass.*;
import static uz.pdp.taskManagement.domain.model.TaskStatus.CREATED;
import static uz.pdp.taskManagement.domain.model.TaskStatus.IN_PROGRESS;

public class BackLeadUI {

    public void BackLeadMenu(User user) {
        while (true) {
            System.out.println("1. Create task\t2. See tasks\t3. Do task\t4. Assign tasks\t0. Exit");
            String choose = scanStr.nextLine();

            switch (choose) {
                case "1" -> createTasks(user);
                case "2" -> seeTasks(user.getId());
                case "3" -> performTask(user.getId());
                case "4" -> assignTask(user);
                case "0" -> {
                    return;
                }
                default -> System.out.println("\nWrong input\n");
            }
        }
    }

    private void seeTasks(UUID id) {
        System.out.println();
        int i = 1;
        for (TaskDTO task : taskService.getUserTasks(id)) {
            System.out.println(i++ + ". Title : " +task.title() + " | Descr : "+ task.description() +
                    " | Status : " + task.status());
            System.out.println("--------------------------------------------------------------------------");
        }
        System.out.println();
    }

    private void createTasks(User user) {
        System.out.print("Enter task title : ");
        String title = scanStr.nextLine();

        System.out.print("Enter task description : ");
        String description = scanStr.nextLine();

        Task task = new Task.TaskBuilder()
                .setTitle(title)
                .setDescription(description)
                .setOwnerId(user.getId())
                .setStatus(CREATED)
                .setTaskRole(TaskRoles.BACKEND)
                .build();
        ResponseDTO<Task> response = taskService.add(task);
        System.out.println(response.getMessage());
    }

    private void performTask(UUID id) {
        seeTasks(id);
        int x = 1;
        System.out.print("Choose task: ");
        int index = scanNum.nextInt();
        if(index == 0){
            return;
        } else if(index < 0 || index > taskService.getUserTasks(id).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(taskService.getUserTasks(id).get(--index).id());
        task.setStatus(IN_PROGRESS);
        task.setTaskRole(TaskRoles.TESTING);
        System.out.println("\nDone\n");
    }

    private void seeBackEndDevs(){
        if(userService.getOnlyDev(Roles.BACKDEV).isEmpty()){
            System.out.println("\nCurrently, there is no any back-end developer\n");
            return;
        }
        int i = 1;
        for (User user : userService.getOnlyDev(Roles.BACKDEV)) {
            System.out.println(i++ + ". " + user.getName());
            System.out.println("-------------------------------------------------");
        }
    }

    private void assignTask(User user){
        seeTasks(user.getId());
        if(taskService.getUserTasks(user.getId()).isEmpty())
            return;
        System.out.print("Choose task you want to assign (back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice < 0 || choice > taskService.getUserTasks(user.getId()).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(taskService.getUserTasks(user.getId()).get(--choice).id());
        System.out.println();
        assignToDev(task);
    }

    private void assignToDev(Task task){
        ArrayList<User> backDevs = userService.getOnlyDev(Roles.BACKDEV);
        seeBackEndDevs();
        if(backDevs.isEmpty())
            return;
        System.out.print("\nChoose worker (back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice < 0 || choice > backDevs.size()){
            System.out.println("\nWrong input\n");
            return;
        }
        task.setReceiverId(backDevs.get(--choice).getId());
        System.out.println("\nSuccessfully assigned\n");
    }
}

