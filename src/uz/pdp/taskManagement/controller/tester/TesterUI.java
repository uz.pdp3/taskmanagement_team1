package uz.pdp.taskManagement.controller.tester;

import uz.pdp.taskManagement.domain.dto.ResponseDTO;
import uz.pdp.taskManagement.domain.dto.TaskDTO;
import uz.pdp.taskManagement.domain.model.Task;
import uz.pdp.taskManagement.domain.model.TaskRoles;
import uz.pdp.taskManagement.domain.model.TaskStatus;
import uz.pdp.taskManagement.domain.model.User;

import java.util.UUID;

import static uz.pdp.taskManagement.controller.UtilClass.*;
import static uz.pdp.taskManagement.domain.model.TaskStatus.CREATED;
import static uz.pdp.taskManagement.domain.model.TaskStatus.IN_PROGRESS;

public class TesterUI {
    public void testerUI(User user){
        byte choice = 0;
        while(true) {
            System.out.println("1. Do task\t 2. See task\t 3. Create task for leads\t 0.Exit");
            choice = scanNum.nextByte();
            switch (choice){
                case 1 -> performTask(user.getId());
                case 2 -> seeTasks(user.getId());
                case 3 -> createTaskForLeads(user);
                case 0 -> {
                    return;
                }
                default -> System.out.println("\nWrong input\n");
            }
        }
    }

    private void createTaskForLeads(User user) {
        TaskRoles role = null;
        System.out.println("\n1. Back-end\t 2. Front-end\t 0.Back");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice == 1)
            role = TaskRoles.BACKEND;
        else if(choice == 2)
            role = TaskRoles.FRONTEND;
        else {
            System.out.println("\nWrong input\n");
            return;
        }
        System.out.print("Enter task title : ");
        String title = scanStr.nextLine();

        System.out.print("Enter task description : ");
        String description = scanStr.nextLine();

        Task task = new Task.TaskBuilder()
                .setTitle(title)
                .setDescription(description)
                .setOwnerId(user.getId())
                .setStatus(CREATED)
                .setTaskRole(role)
                .build();
        ResponseDTO<Task> response = taskService.add(task);
        System.out.println(response.getMessage());
        System.out.println("Assign this project to any lead\n");
        showLeads(role);
        assignToLead(task, role);
    }

    private void assignToLead(Task task, TaskRoles role){
        if(userService.getOnlyLeads(role).isEmpty()){
            return;
        }
        System.out.print("Choose (back=0) : ");
        byte choice = scanNum.nextByte();
        if(choice == 0)
            return;
        else if(choice < 0 || choice > userService.getOnlyLeads(role).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        task.setReceiverId(userService.getOnlyLeads(role).get(--choice).getId());
        System.out.println("\nSuccessfully assigned\n");
    }

    private void showLeads(TaskRoles role){
        if(userService.getOnlyLeads(role).isEmpty()){
            System.out.println("\nCurrently, there is no any lead on this field\n");
            return;
        }
        int i = 1;
        for (User onlyLead : userService.getOnlyLeads(role)) {
            System.out.println(i++ + ". " + onlyLead.getName());
            System.out.println("--------------------------------------------------");
        }
        System.out.println();
    }

    private void seeTasks(UUID id) {
        System.out.println();
        int i = 1;
        for (TaskDTO task : taskService.getUserTasks(id)) {
            System.out.println(i++ + ". Title : " +task.title() + " | Descr : "+ task.description() +
                    " | Status : " + task.status());
            System.out.println("--------------------------------------------------------------------------");
        }
        System.out.println();
    }

    private void performTask(UUID id) {
        seeTasks(id);
        int x = 1;
        System.out.print("Choose task: ");
        int index = scanNum.nextInt();
        if(index == 0){
            return;
        } else if(index < 0 || index > taskService.getUserTasks(id).size()){
            System.out.println("\nWrong input\n");
            return;
        }
        Task task = taskService.getById(taskService.getUserTasks(id).get(--index).id());
        task.setStatus(TaskStatus.DONE);
        System.out.println("\nDone\n");
    }
}
