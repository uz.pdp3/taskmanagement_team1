Task Management system:

Users: 
	Manager
	Backend Lead
	Frontend Lead
	Backend/Frontend dev
	Tester
	User

Tasks: Backend, Frontend, Testing

Manager:
	create any type of task
	can assign to anyone (BE for BE, FE for FE)
	can see all users (workers)
	one user and his/her tasks
	change role of user

User:
	sign in/ sign up (role should be user)

BE/FE lead:
	create and assign task for BE/FE devs
	pass task to devs(status should be ASSIGNED)
	do task
	see tasks

BE/FE devs
	do task
	see tasks
Tester:
	do task
	see tasks
	create task for leads

TaskStatus:
	CREATED
	ASSIGNED
	IN_PROGRESS
	DONE
	BLOCK
	CANCELED
	
Task cannot be move from Assigned to Done directly



